package com.example.BackendTourOfHeroes.models;

import lombok.*;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Data
@Table(name = "HERO")
@RequiredArgsConstructor
@NoArgsConstructor
public class Hero {
    @Id
    @Column(name = "ID", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NonNull
    @Column(name = "NAME")
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (!(o instanceof Hero))
            return false;
        Hero hero = (Hero) o;
        return Objects.equals(this.id, hero.id) && Objects.equals(this.name, hero.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id, this.name);
    }

    @Override
    public String toString() {
        return "Hero{" + "id=" + this.id + ", name='" + this.name + '\'' + '}';
    }
}
