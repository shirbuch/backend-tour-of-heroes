package com.example.BackendTourOfHeroes.controllers;

import com.example.BackendTourOfHeroes.bl.HeroHandler;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import com.example.BackendTourOfHeroes.models.Hero;
import javax.inject.Inject;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/heroes")
public class HeroController {
    private final HeroHandler heroHandler;

    @Inject
    HeroController(HeroHandler heroHandler) {
        this.heroHandler = heroHandler;
    }

    @GetMapping("/allHeroes")
    List<Hero> getAllHeroes() {
        return heroHandler.getAllHeroes();
    }

    @PostMapping("/newHero")
    Hero createNewHero(@RequestBody String name) {
        return heroHandler.addNewHero(name);
    }

    @GetMapping("/{id}")
    Hero getHero(@PathVariable Long id) {
        return heroHandler.getHeroById(id);
    }

    @PutMapping("/{id}")
    Hero updateHero(@RequestBody Hero newHero, @PathVariable Long id) {
        return heroHandler.updateHeroById(id, newHero);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    void deleteHero(@PathVariable Long id) {
        heroHandler.deleteHeroById(id);
    }

    @GetMapping("/heroByTerm/{term}")
    List<Hero> getHeroesByTerm(@PathVariable String term) {
        return heroHandler.getHeroesByTerm(term);
    }
}