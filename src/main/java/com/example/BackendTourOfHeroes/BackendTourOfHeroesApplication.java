package com.example.BackendTourOfHeroes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendTourOfHeroesApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendTourOfHeroesApplication.class, args);
	}
}
