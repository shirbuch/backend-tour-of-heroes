package com.example.BackendTourOfHeroes.bl;

import com.example.BackendTourOfHeroes.exceptions.HeroAlreadyExistsException;
import com.example.BackendTourOfHeroes.exceptions.HeroNotSavedException;
import com.example.BackendTourOfHeroes.exceptions.HeroNotFoundException;
import com.example.BackendTourOfHeroes.models.Hero;
import com.example.BackendTourOfHeroes.repositories.HeroRepository;
import org.springframework.transaction.annotation.Transactional;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;
import java.util.stream.Collectors;

@Named
public class HeroHandler {
    private final HeroRepository heroRepository;

    @Inject
    public HeroHandler(HeroRepository heroRepository) {
        this.heroRepository = heroRepository;
    }

    public List<Hero> getAllHeroes() {
        return heroRepository.findAll();
    }

    @Transactional
    public Hero addNewHero(String name) {
        List<Hero> allHeroes = heroRepository.findAll();
        for (Hero hero : allHeroes) {
            if (hero.getName().equalsIgnoreCase(name)) {
                throw new HeroAlreadyExistsException(name);
            }
        }

        heroRepository.saveNew(name);
        return heroRepository.findByName(name).orElseThrow(() -> new HeroNotSavedException(name));
    }

    public Hero getHeroById(Long id) {
        return heroRepository.findById(id)
                .orElseThrow(() -> new HeroNotFoundException(id));
    }

    public Hero updateHeroById(Long id, Hero newHero) {
        return heroRepository.findById(id)
                .map(hero -> {
                    hero.setName(newHero.getName());
                    return heroRepository.save(hero);
                })
                .orElseGet(() -> {
                    newHero.setId(id);
                    return heroRepository.save(newHero);
                });
    }

    public void deleteHeroById(Long id) {
        if (heroRepository.findById(id).isPresent()) {
            heroRepository.deleteById(id);
        }
    }

    public List<Hero> getHeroesByTerm(String term) {
        List<Hero> allHeroes = heroRepository.findAll();

        return allHeroes.stream()
                .filter(hero -> hero.getName().toLowerCase().contains(term.toLowerCase()))
                .collect(Collectors.toList());
    }
}
