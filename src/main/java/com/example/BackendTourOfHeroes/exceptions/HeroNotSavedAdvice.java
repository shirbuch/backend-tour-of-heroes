package com.example.BackendTourOfHeroes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class HeroNotSavedAdvice {
    @ResponseBody
    @ExceptionHandler(HeroNotFoundException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String heroNotFoundExceptionHandler(HeroNotFoundException ex) {
        return ex.getMessage();
    }
}
