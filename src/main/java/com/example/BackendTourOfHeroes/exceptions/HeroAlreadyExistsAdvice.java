package com.example.BackendTourOfHeroes.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
public class HeroAlreadyExistsAdvice {
    @ResponseBody
    @ExceptionHandler(HeroAlreadyExistsException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String heroAlreadyExistsHandler(HeroAlreadyExistsException ex) {
        return ex.getMessage();
    }
}
