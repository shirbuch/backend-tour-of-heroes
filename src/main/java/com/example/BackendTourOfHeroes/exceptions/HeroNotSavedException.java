package com.example.BackendTourOfHeroes.exceptions;

public class HeroNotSavedException extends RuntimeException {
    public HeroNotSavedException(String name) {
        super("Hero " + name + " was not added");
    }
}
