package com.example.BackendTourOfHeroes.exceptions;

public class HeroAlreadyExistsException extends RuntimeException {
    public HeroAlreadyExistsException(String name) {
        super("Hero " + name + " already exists");
    }
}
