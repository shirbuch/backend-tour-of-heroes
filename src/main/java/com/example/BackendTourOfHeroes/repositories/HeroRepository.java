package com.example.BackendTourOfHeroes.repositories;

import com.example.BackendTourOfHeroes.models.Hero;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import java.util.Optional;

@Repository
public interface HeroRepository extends JpaRepository<Hero, Long> {
    @Transactional
    @Modifying(clearAutomatically = true, flushAutomatically = true)
    @Query(value = "INSERT INTO HERO (name) VALUES (:name)", nativeQuery = true)
    int saveNew(@Param("name") String name);

    Optional<Hero> findByName(String name);
}
